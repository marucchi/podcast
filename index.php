<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=4.0,user-scalable=yes">
  <title>my podcast</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <style>
    body { font-size: 13px; }
    ul { padding: 0; margin: 0; }
    li {
      width: 100px; height: 80px;
      background-color: #eee;
      display: inline-block;
    }
  </style>
</head>
<body>
  <p>For iPod</p>
  <ul>
    <li><a href="pcast://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/basic1.xml">基礎英語1</a></li>
    <li><a href="pcast://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/basic2.xml">基礎英語2</a></li>
    <li><a href="pcast://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/basic3.xml">基礎英語3</a></li>
    <li><a href="pcast://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/kaiwa.xml">ラジオ英会話</a></li>
    <li><a href="pcast://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/timetrial.xml">英会話タイムトライアル</a></li>
    <li><a href="pcast://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/enjoy.xml">エンジョイ・シンプル・イングリッシュ</a></li>
    <li><a href="pcast://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/kouryaku.xml">攻略！英語リスニング</a></li>
    <li><a href="pcast://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/business.xml">ビジネス英語</a></li>
    <li><a href="pcast://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/yomu.xml">英語で読む村上春樹</a></li>
  </ul>

  <p>for android</p>
  <ul>
    <li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/basic1.xml">基礎英語1</a></li>
    <li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/basic2.xml">基礎英語2</a></li>
    <li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/basic3.xml">基礎英語3</a></li>
    <li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/kaiwa.xml">ラジオ英会話</a></li>
    <li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/timetrial.xml">英会話タイムトライアル</a></li>
    <li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/enjoy.xml">エンジョイ・シンプル・イングリッシュ</a></li>
    <li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/kouryaku.xml">攻略！英語リスニング</a></li>
    <li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/business.xml">ビジネス英語</a></li>
    <li><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/podcast/yomu.xml">英語で読む村上春樹</a></li>
  </ul>

  <!-- ul>
    <li><a href="feed://153.149.10.183/podcast/basic1.xml">basic1</a></li>
  </ul>
  <ul>
    <li><a href="itms-pcast://153.149.10.183/podcast/basic1.xml">basic1</a></li>
  </ul>
  <ul>
    <li><a href="itms-pcasts://153.149.10.183/podcast/basic1.xml">basic1</a></li>
  </ul>
  <ul>
    <li><a href="itms-podcast://153.149.10.183/podcast/basic1.xml">basic1</a></li>
  </ul>
  <ul>
    <li><a href="itms-podcasts://153.149.10.183/podcast/basic1.xml">basic1</a></li>
  </ul>
  <ul>
    <li><a href="itms-pcast://153.149.10.183/podcast/basic1.xml">basic1</a></li>
    <li><a href="itms-pcast://153.149.10.183/podcast/basic2.xml">basic2</a></li>
    <li><a href="itms-pcast://153.149.10.183/podcast/basic3.xml">basic3</a></li>
  </ul -->

</body>
</html>

