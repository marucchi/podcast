<?php

// グローバル側のホスト名またはIP
$datahost = 'siosite.info';

// データファイルを置いてあるパスへのURL
$dataurl = "http://{$datahost}/podcast/audio/";

// xmlにリストする番組数の上限
$maxitems = 10;

 // 古いファイルは消す
$purgeoldfile = true;


// パス
$scriptsdir = dirname(__FILE__) . '/';
$xmldir = dirname($scriptsdir) . '/';
$datadir = "{$xmldir}audio/";

// 番組のキーと名称
$programs = array(
		'basic1'=>'基礎英語１',
		'basic2'=>'基礎英語２',
		'basic3'=>'基礎英語３',
		'kaiwa'=>'ラジオ英会話',
		'timetrial'=>'英会話タイムトライアル',
		'enjoy'=>'エンジョイ・シンプル・イングリッシュ',
		'kouryaku'=>'攻略！英語リスニング',
		'business'=>'ビジネス英語',
		'yomu'=>'英語で読む村上春樹',
		);

$ns_itunes = 'http://www.itunes.com/DTDs/Podcast-1.0.dtd';

// 引数に番組名が指定してあれば、その番組のみ作成する
if (count($argv) == 2) {
	if (array_key_exists($argv[1], $programs)) {
		$programs = array($argv[1] => $programs[$argv[1]] );
	} else {
		_log('引数に指定されたタイトルが見つかりません');
		exit(1);
	}
}

foreach ($programs as $title => $name) {

	// ファイルリストを作成
	$files = recentfiles($title, $name, $maxitems);

	// ファイルリストからxmlを作成
	$xml = makepodxml($title, $name, $files);

	// xmlを整形して出力
	$dom = new DOMDocument('1.0', 'utf-8');
	$node = $dom->importNode(dom_import_simplexml($xml), true);
	$dom->appendChild($node);
	$dom->preserveWhiteSpace = false;
	$dom->formatOutput = true;
	$dom->save("{$xmldir}{$title}.xml");

	_log("$title($name) を更新しました");
}

// ファイルリストを作成
function recentfiles($title, $name, $maxitems) {
	global $purgeoldfile;
	global $datadir;
	$ext = '.m4a';

	$files = array();

	$allfiles = scandir($datadir);
	foreach($allfiles as $file) {
		if (is_file($datadir.$file) && (strpos($file, $title) === 0) &&
			(strpos($file, $ext) == (strlen($file) - strlen($ext)))) {
			$files[] = $file;
		}
	}

	asort($files);

	if (count($files) > $maxitems) {
		if ($purgeoldfile) {
			$purgefiles = array_splice($files, 0, count($files) - $maxitems);
			foreach($purgefiles as $file) {
				_log("unlink : $file");
				unlink($datadir.$file);
			}
		}
		$files = array_splice($files, count($files) - $maxitems);
	}

	return $files;
}

// ファイルリストからxmlを作成
function makepodxml($title, $name, $files) {
	global $ns_itunes;
	global $datahost;
	global $dataurl;
	global $datadir;
	$outputfiles = "{$title}.xml";

	$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>'.
	"<rss xmlns:itunes=\"{$ns_itunes}\" version=\"2.0\">".
	'<channel/></rss>');


	$xml->channel->addChild('title', $name);
	$xml->channel->addChild('link', "{$datahost}podcast");
	$xml->channel->addChild('description', $name);
	$xml->channel->addChild('author', 'nhk');

	$image = $xml->channel->addChild('image', null, $ns_itunes);
	$image->addAttribute('href', "{$datahost}podcast/images/{$title}.png");

	$xml->channel->addChild('lastBuildDate', date('F j, Y, g:i a'));
	$xml->channel->addChild('copyright', $title);
	$xml->channel->addChild('language', 'ja');

	$count = count($files);
	$i = 0;

	foreach($files as $file) {
		$xml->channel->item[$i]->title = title($file);
		$xml->channel->item[$i]->description = $name;
		$xml->channel->item[$i]->pubDate = pubDate($file);

		$enclosure = $xml->channel->item[$i]->addChild('enclosure', '');
		$enclosure->addAttribute('url', "{$dataurl}{$file}");
		$enclosure->addAttribute('length', filesize($datadir.$file));
		$enclosure->addAttribute('type', 'audio/aac');

		$i++;
	}

	return $xml; 
}

function title($file) {
        global $programs;

        // 拡張子を除く
        $pos = strpos($file, '.');
        if ($pos !== false) {
                $file = substr($file, 0, $pos);
        }

        list($title, $date) = preg_split('/_/', $file, 2);

        if (isset($programs[$title])) {
                $title = $programs[$title];
        }

        if (isset($date)) {
                $dates = preg_split('/-/', $date, 3);
                if (count($dates) == 3) {
                        $date = "{$dates[0]}年{$dates[1]}月{$dates[2]}日";
                }
        }

        return "$title $date";
}

function description($file) {
	$pos = strpos($file, '.');
	if ($pos !== false) {
		$file = substr($file, 0, $pos);
	}
	return $file;
}

function pubDate($file) {
	$pos = strpos($file, '.');
	if ($pos !== false) {
		$file = substr($file, 0, $pos);
		$pos = strpos($file, '_');
		if ($pos !== false) {
			$file = substr($file, ++$pos);
		}
	}
	return $file;
}

function _log($message, $level = LOG_INFO) {
	if (!is_string($message)) {
		$message = json_encode($message);
	}

	// echo $message . PHP_EOL;
	syslog($level, '['.basename(__FILE__)."] $message");
}
