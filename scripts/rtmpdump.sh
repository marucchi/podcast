#!/bin/bash

title=${1:-basic1}
dumpfile=`date '+_%Y-%m-%d.aac'`
convertfile=`date '+_%Y-%m-%d.m4a'`

second=${2:-1}

# 保存先ディレクトリに移動します
cd /var/www/html/podcast/audio/


# 指定秒数ストリーミングを受信し、ファイルに保存します
/usr/local/bin/rtmpdump -r "rtmpe://netradio-r2-flash.nhk.jp" -y "NetRadio_R2_flash@63342" -a "live" -W "http://www3.nhk.or.jp/netradio/files/swf/rtmpe.swf" -v -o ${title}${dumpfile} -B ${second}

# 保存できたら .aac ファイルから .m4a に変換します
# エンコーディングはいじりません。
if [ $? = 0 ]; then
  /usr/local/bin/ffmpeg -y -i ${title}${dumpfile} -acodec copy ${title}${convertfile}
  
# aacファイルが不要なら削除します
if [ $? = 0 ]; then
  /bin/rm ${title}${dumpfile}
fi
  
# podcast.xmlを更新します
/usr/bin/php /var/www/html/podcast/scripts/makepodcast.php ${title}
fi
